# PEC 3 Modding y creación de niveles

Memoria detallada adjunta en PDF con detalles del proyecto y su explicación.

# Diseño de niveles 3
Desarrollo SOKOBAN Unity y editor de niveles

#Jonathan Gómez Berengueras

# Presentación del proyecto

Sokoban es un clásico rompecabezas inventado en Japón, normalmente implementado como videojuego. El juego original fue creado por Hiroyuki Imabayashi, que en 1980 ganó con su juego una competición contra un ordenador.
Un juego de puzles donde controlamos a un personaje que debe empujar cajas y llevarlas hacia posiciones específicas del nivel.

# Como jugar?

Controles jugador: WASD
Controles camara en el editor: WASD


Alumne: Jonathan Gómez Berengueras

Entrega de la PAC3.

LINK repostori GITLAB: https://gitlab.com/JonathanGomez/pac3_jonathangomez_sokoban

LINK Vídeo: https://youtu.be/nj8Jg55n6mw